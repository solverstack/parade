all: export
	
export: 	LabBook.org
		emacs -batch -l ~/.emacs.d/init.el $^ --funcall org-babel-tangle
clean:
		rm -f scripts/*.sh scripts/*.txt

