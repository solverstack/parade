#+TITLE:     Experimental Framework for qr_mumps
#+AUTHOR:    Ian Masliah
#+DATE: 2017
#+STARTUP: overview indent


This project will provide an experimental framework for experimental design on 
parallel architectures in the context of the qr_mumps library ([[http://buttari.perso.enseeiht.fr/qr_mumps/).]]
 
Please read LabBook.org for further details.
 
